## Dependencies

- Node.js > 6.4.0

## Usage

Clone the directory from bitbucket and cd into **Dedocx**
Run `npm install`

Take a .docx file and output as html
The first argument is the full path of the docx document you want to convert.
The second argument is the full path to save the converted html to.
```
$ ./index.js "c:\Users\<you>\docx-file.docx" "c:\Users\<you>\mynamedfile.shtml"
```

