const pretty = require('pretty')
const mammoth = require('mammoth')
const path = require('path')
const fs = require('fs')

let docxPath = path.normalize(process.argv.slice(2)[0].toString())
let outPath = path.normalize(process.argv.slice(3)[0].toString())
console.log('docxPath was ', docxPath)
console.log('outPath was ',outPath)

function docxToHtml() {
	mammoth.convertToHtml({path: docxPath})
		.then(escapeHtml)
		.then(writePretty)
		.done()
}

function escapeHtml(html) {
	return html.value
		.replace(/&/g, '&amp;')
		.replace(/©/g, '&copy;')
		.replace(/®/g, '&reg;')
		// build out the conversion list for french special character
		// while avoiding html entities

}

function writePretty(html) {
	if (!html) {
		console.error('no html')
	} else {
		fs.writeFile(outPath, pretty(html), function(err) {
			if (err) {
				return console.log(err)
			}
		})
	}

	console.log('======================\n\n\n\n')
	console.log(pretty(html))
	console.log(`->>saved to ${outPath}`)
}

docxToHtml()
